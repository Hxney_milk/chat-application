import { User } from './User';

export class AuthService {
  private users: User[] = [];

  register(username: string, password: string, email: string): boolean {
    if (this.users.find(user => user.username === username)) {
      return false;
    }
    this.users.push(new User(username, password, email));
    return true;
  }

  login(username: string, password: string): User | null {
    return this.users.find(user => user.username === username && user.password === password) || null;
  }
}
