const container = document.getElementById('container')!;
const registerForm = document.getElementById('register-form') as HTMLFormElement;
const loginForm = document.getElementById('login-form') as HTMLFormElement;
const loginButton = document.getElementById('login') as HTMLButtonElement;
const registerButton = document.getElementById('register') as HTMLButtonElement;

registerButton.addEventListener('click', () => {
  container.classList.add("right-panel-active");
});

loginButton.addEventListener('click', () => {
  container.classList.remove("right-panel-active");
});

registerForm.addEventListener('submit', async (e) => {
  e.preventDefault();
  const username = (document.getElementById('register-username') as HTMLInputElement).value;
  const email = (document.getElementById('register-email') as HTMLInputElement).value;
  const password = (document.getElementById('register-password') as HTMLInputElement).value;

  const res = await fetch('/auth/register', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ username, password, email })
  });
  const data = await res.text();
  alert(data);
  if (res.status === 201) {
    container.classList.remove("right-panel-active");
  }
});

loginForm.addEventListener('submit', async (e) => {
  e.preventDefault();
  const username = (document.getElementById('login-username') as HTMLInputElement).value;
  const password = (document.getElementById('login-password') as HTMLInputElement).value;

  const res = await fetch('/auth/login', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ username, password })
  });
  const data = await res.text();
  if (res.status === 200) {
    localStorage.setItem('user', JSON.stringify({ username }));
    window.location.href = 'src/chat/chat.html';  
  } else {
    alert(data);
  }
});
