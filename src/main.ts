const user = localStorage.getItem('user');

if (user) {
  window.location.href = '/src/chat/chat.html';
} else {
  window.location.href = '/src/login/login.html';
}
