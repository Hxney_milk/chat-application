import { Request, Response } from 'express';
import { AuthService } from '../login/AuthService';

const authService = new AuthService();

export const register = (req: Request, res: Response) => {
  const { username, password, email } = req.body;
  if (authService.register(username, password, email)) {
    res.status(201).send('User registered successfully');
  } else {
    res.status(400).send('Username already exists');
  }
};

export const login = (req: Request, res: Response) => {
  const { username, password } = req.body;
  const user = authService.login(username, password);
  if (user) {
    req.session.user = user;
    res.status(200).send('Login successful');
  } else {
    res.status(401).send('Invalid username or password');
  }
};

export const logout = (req: Request, res: Response) => {
  req.session.destroy((err) => {
    if (err) {
      return res.status(500).send('Could not log out');
    } else {
      res.status(200).send('Logout successful');
    }
  });
};
