import express from 'express';
import bodyParser from 'body-parser';
import session from 'express-session';
import authRoutes from './routes/authRoutes';

const app = express();

app.use(bodyParser.json());
app.use(session({
  secret: 'my-secret',
  resave: false,
  saveUninitialized: true
}));

app.use('/auth', authRoutes);

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/login/login.html');
});

export default app;
