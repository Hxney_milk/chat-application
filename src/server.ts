import http from 'http';
import { Server } from 'socket.io';
import app from './app';
import { ChatService } from './chat/ChatService';
import { Message } from './chat/Message';

const server = http.createServer(app);
const io = new Server(server);
const chatService = new ChatService();

io.on('connection', (socket) => {
  socket.on('joinRoom', (roomId, user) => {
    let chatRoom = chatService.getRoomById(roomId);
    if (!chatRoom) {
      chatRoom = chatService.createRoom(roomId);
    }
    chatRoom.addParticipant(user);
    socket.join(roomId);
  });

  socket.on('message', (roomId, messageContent, sender) => {
    const chatRoom = chatService.getRoomById(roomId);
    if (chatRoom) {
      const message = new Message(messageContent, new Date(), sender);
      chatService.sendMessage(chatRoom, message);
      io.to(roomId).emit('message', message);
    }
  });
});

server.listen(3000, () => {
  console.log('Server is running on port 3000');
});
