import { ChatRoom } from './ChatRoom';
import { Message } from './Message';
import { User } from '../login/User';

export class ChatService {
  private chatRooms: ChatRoom[] = [];

  createRoom(name: string): ChatRoom {
    const chatRoom = new ChatRoom(name);
    this.chatRooms.push(chatRoom);
    return chatRoom;
  }

  getRoomById(id: string): ChatRoom | undefined {
    return this.chatRooms.find(room => room.id === id);
  }

  sendMessage(chatRoom: ChatRoom, message: Message) {
    chatRoom.addMessage(message);
  }

  getMessages(chatRoom: ChatRoom): Message[] {
    return chatRoom.getMessages();
  }
}
