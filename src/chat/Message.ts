import { User } from '../login/User';

export class Message {
  constructor(
    public contents: string,
    public timestamp: Date,
    public sender: User
  ) {}
}
