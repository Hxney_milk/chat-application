import { User } from '../login/User';
import { Message } from './Message';

export class ChatRoom {
  public id: string;
  public participants: User[] = [];
  private messages: Message[] = [];

  constructor(public name: string) {
    this.id = Math.random().toString(36).substring(2, 15);
  }

  addParticipant(user: User) {
    this.participants.push(user);
  }

  removeParticipant(user: User) {
    this.participants = this.participants.filter(participant => participant.username !== user.username);
  }

  addMessage(message: Message) {
    this.messages.push(message);
  }

  getMessages(): Message[] {
    return this.messages;
  }
}
