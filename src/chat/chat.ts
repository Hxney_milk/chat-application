import { io } from "socket.io-client";

const socket = io();

const messagesDiv = (document.getElementById('messages') as HTMLElement)!;
const messageForm = (document.getElementById('message-form') as HTMLFormElement)!;
const messageInput = (document.getElementById('message-input') as HTMLInputElement)!;
const logoutButton = (document.getElementById('logout-button') as HTMLElement)!;

if (!user) {
  window.location.href = '../login/login.html';
} else {
  socket.emit('joinRoom', 'general', user);
}
messageForm.addEventListener('submit', (e) => {
  e.preventDefault();
  const messageContent = messageInput.value;
  socket.emit('message', 'general', messageContent, user);
  messageInput.value = '';
});

socket.on('message', (message: any) => {
  const messageElement = document.createElement('div');
  messageElement.innerText = `${message.sender.username} (${new Date(message.timestamp).toLocaleTimeString()}): ${message.contents}`;
  messagesDiv.appendChild(messageElement);
});

logoutButton.addEventListener('click', () => {
  localStorage.removeItem('user');
  window.location.href = '../login/login.html';
});
